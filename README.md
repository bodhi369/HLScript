# HLScript

### 介绍
HLScript管理系统

解决PCB工程软件genesis 以及 incam 软件脚本、配置文件不能及时更新问题

本HLScript管理系统,是无限制且免费的

### 安装教程

#### 服务端安装
1.  还原HLScript.bak数据库 到 Microsoft SQL Server 2008 及以上。
2.  修改server/config/conf.local.yaml
    ```
    psn: server=数据库地址;user id=数据库用户;password=数据库密码;database=还原的数据库名;encrypt=disable  
    例如:
    psn: server=192.168.10.6;user id=sa;password=999.com;database=HLSCript;encrypt=disable
    ```
3.  启动服务
    * 运行 server/api.exe
    
    ```
         __ __   __
        / // /  /   __   .   __  ___
       / _  /__ --/   |/ |  |__|  |
      /_//____/__/___ |  |  |     | v1.0.0
      本HLScript管理系统,是无限制且免费的
      https://gitee.com/bodhi369/HLScript
      ___________________________________________
      
      ⇨ http server started on [::]:12233
    ```

    * 运行 server/web.exe
    
    ```
         __ __   __
        / // /  /   __   .   __  ___
       / _  /__ --/   |/ |  |__|  |
      /_//____/__/___ |  |  |     | v1.0.0
      本HLScript管理系统,是无限制且免费的
      https://gitee.com/bodhi369/HLScript
      ___________________________________________
      
      ⇨ http server started on [::]:12234    
    ```

4. 打开浏览器（最好为谷歌chrome浏览器） 

   ```
   http://server所在的ip地址:12234
   例如:
   http://192.168.10.6:12234
   ```
   
5.  登陆并配置
    
    * 用户密码为 HLScript

#### 客户端安装
1. 运行client/HLCam.exe 进行安装

2. 点击 配置并重启

3. 运行 HLUpdate

4. 配置 Url 

   ```
   http://server所在的ip地址:12233
   例如:
   http://192.168.10.6:12233
   ```

### HLScript配置教程

1.  修改公司名称
   
   * 系统管理->公司管理->编辑
   
2. 修改用户类型
   
* 系统管理->基础数据->编辑  改成需要的
  
3. 配置终端、脚本信息

   ##### 1.  终端信息

   * 脚本管理->终端信息->编辑 或 添加
   * 用于存储 $GENESIS_DIR/ 下的内容 如：symbol forms hooks ... 都是选择 "点击上传文件夹"
   * 路径 例如 都是选择"点击上传文件夹" 选择 "symbol"  $GENESIS_DIR**/fw/lib**/symbol  路径为 /fw/lib

   ##### 2.  脚本信息

   *  脚本管理->脚本信息->编辑 或 添加
   *  用于存储 $INCAM_SERVER/site_data/scripts/ 下的内容 都是选择 "点击上传文件"
   * 路径 例如 都是选择"点击上传文夹" 选择 "copy" $INCAM_SERVER**/site_data/scripts**/copy 路径为 /site_data/scripts

  4.  配置流程信息

     *  脚本管理->流程信息->编辑 或 添加
     *  梳理工程制流程

5.  配置流程脚本

   *  脚本管理->流程脚本->编辑
   *  绑定流程 与 脚本之前的关系


#### 终端更新

* 每次有更新，配置完成后，让客户端运行HLUpdate，进行更新

#### 文件信息： （请务必核对文件信息，不正确切勿使用！）
###### 文件：api.exe
大小: 32431005 字节

MD5: FDC4A5D0054913488021AE52A88B399C

SHA1: 288EE9F2EDFD95A11D8B6FE5D2C3E74BA271B471

CRC32: C507ED2D

###### 文件：web.exe
大小: 13764096 字节

MD5: 6AF15CACC0249D2C16382C8B9CC60C56

SHA1: 943FC853ACE364EAAE121047E7F0D0D5A46592F0

CRC32: 46D11AEC

###### 文件：HLCam.exe
大小: 38321627 字节

MD5: A852F8A322F95A2FB9F702634F00A144

SHA1: 2DCC7E8E4692289B9D00D7AD795045B32DCFB39D

CRC32: 2A06E0D2